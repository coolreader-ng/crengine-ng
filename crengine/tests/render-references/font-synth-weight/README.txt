Render references for synthetic font weight feature.

width: 640
height: 360
bpp: 32
Font: FreeSans, 80 px
Background color: white
TextColor: black
Hinting: None
Text shaping: Simple
Kerning: off
Antialiasing: Gray
Font weight: <various>
