Render references for font-gamma feature.

width: 640
height: 360
bpp: 32
Font: FreeSerif, 20 px
Background color: white
TextColor: black
Hinting: None
Text shaping: Simple
Kerning: off
Antialiasing: Gray
Font Gamma: <various>
