Render references for font-families feature.

width: 640
height: 360
bpp: 32
Fonts: FreeSerif, FreeSans, FreeMono, 20 px
Background color: white
TextColor: black
Hinting: None
Text shaping: Simple
Kerning: off
Antialiasing: Gray
Font Gamma: 1.0
